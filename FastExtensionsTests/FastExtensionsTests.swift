//
//  FastExtensionsTests.swift
//  FastExtensionsTests
//
//  Created by Omer Rahmany on 26/04/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import XCTest

@testable import FastExtensions

class FastExtensionsTests: XCTestCase {
    var company: Company?
    let newCompanyID = "123"
    let newRootExtensionID = "1231"
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        super.setUp()
    }
    
    func testClearDB() throws {
        let allCompaniesDeletedPromise = expectation(description: "Companies deleted successfully")
//        let allExtensionsDeletedPromise = expectation(description: "Extensions deleted successfully")
//        let allOptionsDeletedPromise = expectation(description: "Options deleted successfully")
//        let allExtensionsOptionMembersDeletedPromise = expectation(description: "ExtensionsOptionMembers deleted successfully")
//
        FirebaseGlobalManager.allCompanies.removeValue(completionBlock: { err,ref in
            allCompaniesDeletedPromise.fulfill()
        })
//        FirebaseGlobalManager.allExtensions.removeValue(completionBlock: { err,ref in
//            allExtensionsDeletedPromise.fulfill()
//        })
//        FirebaseGlobalManager.allOptions.removeValue(completionBlock: { err,ref in
//            allOptionsDeletedPromise.fulfill()
//        })
//        FirebaseGlobalManager.allExtensionsOptionMembers.removeValue(completionBlock: { err,ref in
//            allExtensionsOptionMembersDeletedPromise.fulfill()
//        })
//        wait(for: [allCompaniesDeletedPromise, allExtensionsDeletedPromise, allOptionsDeletedPromise, allExtensionsOptionMembersDeletedPromise], timeout: 5)
        wait(for: [allCompaniesDeletedPromise], timeout: 5)
    }
    
    func testAddCompany() throws {
        let rootExtension = Extension(uid: newRootExtensionID, title: "Language", number: "1", isRoot: true, parentUid: newCompanyID, parentExtensionUid: nil)
        let imageUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/7/7a/Bezeq_Logo.svg/1200px-Bezeq_Logo.svg.png"
        company = Company(uid: newCompanyID, name: "Hot", phoneNumber: "012", imageURL: imageUrl, rootExtensionID: newRootExtensionID)
        
        let companyCreatedPromise = expectation(description: "Company created successfully")
        let extensionCreatedPromise = expectation(description: "RootExtension created successfully")
        FirebaseGlobalManager.add(newExtension: rootExtension, completion: { didCreate in
            if didCreate {
                FirebaseGlobalManager.allExtensions.child(self.newRootExtensionID).observe(.value, with: { snapshot in
                    if snapshot.exists() {
                        extensionCreatedPromise.fulfill()
                    } else {
                        XCTFail("Failed to create a new root extension: root extension not found in firebase")
                    }
                })
            }
        })
        
        FirebaseGlobalManager.add(newCompany: company!, completion: { didCreate in
            if didCreate {
                FirebaseGlobalManager.allExtensions.child(self.newRootExtensionID).observe(.value, with: { snapshot in
                    if snapshot.exists() {
                        companyCreatedPromise.fulfill()
                    } else {
                        XCTFail("Failed to create a new company: company not found in firebase")
                    }
                })
            }
        })
        wait(for: [companyCreatedPromise, extensionCreatedPromise], timeout: 5)
    }
    
    func testRemoveCompany() throws {
        let companyDeletedPromise = expectation(description: "Company removed successfully")
        let extensionDeletedPromise = expectation(description: "RootExtension removed successfully")
        FirebaseGlobalManager.removeCompanyFor(uid: newCompanyID, completion: { (didRemove) in
            if didRemove {
                FirebaseGlobalManager.allCompanies.observeSingleEvent(of: .value, with: { snapshot in
                    if !snapshot.hasChild(self.newCompanyID){
                        companyDeletedPromise.fulfill()
                    } else {
                        XCTFail("Failed to remove company: company: \(self.company!) found in firebase")
                    }
                })
                sleep(2) // removing the root extension is done by the firebase functions so we need to wait for them to run
                FirebaseGlobalManager.allExtensions.observeSingleEvent(of: .value, with: { snapshot in
                    if !snapshot.hasChild(self.newRootExtensionID){
                        extensionDeletedPromise.fulfill()
                    } else {
                        XCTFail("Failed to remove root extension: extensionID: \(self.newRootExtensionID) found in firebase")
                    }
                })
            }
        })
        
        wait(for: [companyDeletedPromise, extensionDeletedPromise], timeout: 10)
    }
    
    func testAddCompanyWithManyChildren() throws {
        let rootExtension = Extension(uid: newRootExtensionID, title: "Language", number: "1", isRoot: true, parentUid: newCompanyID, parentExtensionUid: nil)
               let imageUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/7/7a/Bezeq_Logo.svg/1200px-Bezeq_Logo.svg.png"
               company = Company(uid: newCompanyID, name: "Hot", phoneNumber: "012", imageURL: imageUrl, rootExtensionID: newRootExtensionID)
               
    }
    
    //    func testAddAndRemoveCompany() throws {
    //        // This is an example of a functional test case.
    //        // Use XCTAssert and related functions to verify your tests produce the correct results.
    //        company = FirebaseCompanyManager.addCompany(name: "Bezeq", phoneNumber: "012", imageURL: "https://upload.wikimedia.org/wikipedia/en/thumb/7/7a/Bezeq_Logo.svg/1200px-Bezeq_Logo.svg.png", childExtensionNumber: "1", childExtensionTitle: "Language")
    //        let companyCreatedPromise = expectation(description: "Company created successfully")
    //        let companyDeletedPromise = self.expectation(description: "Company root extension was deleted successfully")
    //
    //        if let newCompany = company {
    //            FirebaseGlobalManager.allCompanies.child(newCompany.uid).observeSingleEvent(of: .value, with: { snapshot in
    //                if snapshot.exists() {
    //                    companyCreatedPromise.fulfill()
    //                } else {
    //                    XCTFail("Failed to create a new company: company not found in firebase")
    //                }
    //                FirebaseCompanyManager.removeCompany(uid: newCompany.uid, completion: { didRemove in
    //                    if didRemove {
    //                        FirebaseGlobalManager.allCompanies.observeSingleEvent(of: .value, with: { snapshot in
    //                            if !snapshot.hasChild(newCompany.uid){
    //                                companyDeletedPromise.fulfill()
    //                            } else {
    //                                XCTFail("Failed to remove company: company: \(self.company!) found in firebase")
    //                            }
    //                        })
    //                    }
    //                })
    //            })
    //        } else {
    //            XCTFail("Unable to create a new company")
    //        }
    //        wait(for: [companyCreatedPromise, companyDeletedPromise], timeout: 5)
    //    }
    //
    //    func testRemoveCompanyAndAssociates() throws {
    //        company = FirebaseCompanyManager.addCompany(name: "Bezeq", phoneNumber: "012", imageURL: "https://upload.wikimedia.org/wikipedia/en/thumb/7/7a/Bezeq_Logo.svg/1200px-Bezeq_Logo.svg.png", childExtensionNumber: "1", childExtensionTitle: "Language")
    //
    //        if let newCompany = company {
    //            FirebaseCompanyManager.removeCompany(uid: company!.uid, completion: { didRemove in
    //                if didRemove {
    //                    let companyDeletedPromise = self.expectation(description: "Company root extension was deleted successfully")
    //                    FirebaseGlobalManager.allCompanies.child(newCompany.uid).observeSingleEvent(of: .value, with: { snapshot in
    //                        if !snapshot.hasChild(newCompany.uid){
    //                            companyDeletedPromise.fulfill()
    //                        } else {
    //                            XCTFail("Failed to remove company: company: \(self.company!) found in firebase")
    //                        }
    //                    })
    //
    //                    let companyRootExtensionDeletedPromise = self.expectation(description: "Company root extension was deleted successfully")
    //                    FirebaseGlobalManager.allExtensions.child(newCompany.rootExtensionID!).observeSingleEvent(of: .value, with: { snapshot in
    //                        if !snapshot.hasChild(newCompany.uid){
    //                            companyRootExtensionDeletedPromise.fulfill()
    //                        } else {
    //                            if let rootExtensionID = newCompany.rootExtensionID {
    //                                XCTFail("Failed to remove company root extension: extension: \(rootExtensionID) found in firebase")
    //                            }
    //                        }
    //                    })
    //
    //                    let companyMemberDeletedPromise = self.expectation(description: "Company root extension was deleted successfully")
    //                    FirebaseGlobalManager.allCompanyMembers.child(newCompany.uid).observeSingleEvent(of: .value, with: { snapshot in
    //                        if !snapshot.hasChild(newCompany.uid){
    //                            companyMemberDeletedPromise.fulfill()
    //                        } else {
    //                            XCTFail("Failed to remove company member for company: \(newCompany) found in firebase")
    //                        }
    //                    })
    //
    //                    self.wait(for: [companyDeletedPromise, companyRootExtensionDeletedPromise, companyMemberDeletedPromise], timeout: 5)
    //                }
    //            })
    //        }
    //    }
    
    override func tearDown() {
        super.tearDown()
    }
}
