const functions = require('firebase-functions');

var admin = require("firebase-admin");

let serviceAccount = require('../serviceAccountKey.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://fastextensions.firebaseio.com"
});

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

// Listens for new messages added to /messages/:pushId/original and creates an
// uppercase version of the message to /messages/:pushId/uppercase

let companiesRef = admin.database().ref('companies');
let extensionsRef = admin.database().ref('extensions');
let optionsRef = admin.database().ref('options');
let extensionsOptionMembersRef = admin.database().ref('extensionsOptionMembers');


exports.addOptionMemberToParentExtension = functions.database.ref('/options/{optionId}')
    .onCreate((snapshot, context) => {
        const newOption = snapshot.val();
        var optionID = snapshot.key
        console.log('Option ' + optionID + ' was created: ')
        var parentExtensionID = newOption.parentExtensionID
        console.log('Adding ' + optionID + ' to extension: ' + parentExtensionID)
        return extensionsOptionMembersRef.child(parentExtensionID).child(optionID).set(true);;
    });


exports.removeCompanyAndAssociates = functions.database.ref('/companies/{companyId}')
    .onDelete((snapshot, context) => {
        const company = snapshot.val();
        console.log('Company ' + snapshot.key + ' was deleted: ')
        var companyRootExtensionID = company.rootExtensionID

        console.log('Removing extension: ' + companyRootExtensionID)
        return extensionsRef.child(companyRootExtensionID).remove();
    });

exports.removeExtensionAndAssociates = functions.database.ref('/extensions/{extensionId}')
    .onDelete((snapshot, context) => {
        const extension = snapshot.val();
        console.log('Extension ' + snapshot.key + ' was deleted: ')

        if (extension.isRoot) {
            console.log('Extension ' + snapshot.key + ' is a Root extension')
            companiesRef.child(extension.parentUid).once
        }
        return extensionsOptionMembersRef.child(snapshot.key).once('value').then(extensionsOptionMemberSnapshot => {
            extensionsOptionMemberSnapshot.forEach(optionSnapshot => {
                optionsRef.child(optionSnapshot.key).remove()
                extensionsOptionMembersRef.child(snapshot.key).child(optionSnapshot.key).remove()
            });
        });;
    });


exports.removeOptionsAndAssociates = functions.database.ref('/options/{optionsId}')
    .onDelete((snapshot, context) => {
        // Grab the current value of what was written to the Realtime Database.
        const option = snapshot.val();
        console.log('Option ' + snapshot.key + ' was deleted: ')
        extensionsOptionMembersRef.child(option.parentExtensionID).child(snapshot.key).remove()
        return extensionsRef.child(option.childExtensionID).remove();
    });
