//
//  CompanyListTableViewController.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 26/04/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import UIKit
import Firebase
import FirebaseUI

class CompanyListTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, FIRShowAlertDelegate {
    
    let createCompanySegueIdentifier = "createCompany"
    let showExtensionOptionSegueIdentifier = "showExtensionOption"
    let showLoginSegueIdentifier = "showLogin"
    let showSettingsSegueIdentifier = "showSettings"
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var indicator = UIActivityIndicatorView()
    var companies = [Company]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        self.tableView.keyboardDismissMode = .onDrag
        
        if (Auth.auth().currentUser?.email != "omer.rah2@gmail.com") {
            self.navigationItem.leftBarButtonItem = nil
        }
        
        searchBar.delegate = self
        
        self.activityIndic(true)
        FirebaseGlobalManager.fillCompanies(completion: { result in
            DispatchQueue.main.async {
                self.activityIndic(false)
                self.companies = FirebaseGlobalManager.companies
                self.tableView.reloadData()
            }
        })
        
        self.setUpTestingData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setToolbarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //    override func viewDidDisappear(_ animated: Bool) {
    //        self.companies = []
    //    }
    
    @IBAction func addTapped(_ sender: Any) {
        self.performSegue(withIdentifier: self.createCompanySegueIdentifier, sender: nil)
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.companies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyCell", for: indexPath) as! CompanyTableViewCell
        
        let company = self.companies[indexPath.row]
        cell.configureWithType(company)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "חברה"
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "David", size: 15)
        header.textLabel?.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = tableView.backgroundColor
        
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
        -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                // delete the item here
                let companyIDToDelete = self.companies[indexPath.row].uid
                FirebaseGlobalManager.removeCompanyFor(uid: companyIDToDelete, completion: { didRemove in
                    if didRemove {
                        self.companies.remove(at: indexPath.row)
                        self.tableView.reloadData()
                        completionHandler(true)
                    }
                })
            }
            deleteAction.image = UIImage(systemName: "trash")
            deleteAction.backgroundColor = .systemRed
            
            let editAction = UIContextualAction(style: .normal, title: nil) { (_, _, completionHandler) in
                // edit the item here
                completionHandler(true)
            }
            editAction.image = UIImage(systemName: "pencil")
            editAction.backgroundColor = .systemBlue
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction, editAction])
            return configuration
    }
    
    // MARK: - UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.companies = FirebaseGlobalManager.companies
            //            [self performSelector:@selector(hideKeyboardWithSearchBar:) withObject:searchBar afterDelay:0];
            perform(#selector(hideKeyboardWithSearchBar), with: searchBar, afterDelay: 0)
            tableView.reloadData()
            return
        }
        let searchText = searchText.lowercased()
        let filtered = FirebaseGlobalManager.companies.filter({
            let company = $0
            return (company.name.lowercased().contains(searchText))
        }) // <<<
        self.companies = filtered
        tableView.reloadData()
    }
    
    //    end searching --> Close Keyboard
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        self.searchBar.endEditing(true)
    }
    
    @objc func hideKeyboardWithSearchBar(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func activityIndicator() {
        indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        indicator.style = UIActivityIndicatorView.Style.medium
        indicator.center = self.view.center
        self.view.addSubview(indicator)
    }
    
    func activityIndic(_ shouldShow: Bool) {
        if shouldShow {
            activityIndicator()
            indicator.startAnimating()
            indicator.backgroundColor = .white
        } else {
            indicator.stopAnimating()
            indicator.hidesWhenStopped = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == showExtensionOptionSegueIdentifier) {
            // Get the index path from the cell that was tapped
            let indexPath = self.tableView.indexPathForSelectedRow
            // Get the Row of the Index Path and set as index
            if let index = indexPath?.row {
                let selectedCompany = self.companies[index]
                //                FirebaseGlobalManager.databaseRef = FirebaseGlobalManager.allCompanies.child(selectedCompany.uid)
                FirebaseGlobalManager.currentCompanyID = selectedCompany.uid
                FirebaseGlobalManager.currentExtensionID = selectedCompany.rootExtensionID
            }
        }
    }
    
    
    func setUpTestingData() {
        let newCompanyID = "1"
        let newRootExtensionID = "11"
        let newOption1ID = "111"
        let newOption1RootExtensionID = "1111"
        let newOption2ID = "112"
        let imageUrl = "https://upload.wikimedia.org/wikipedia/en/thumb/7/7a/Bezeq_Logo.svg/1200px-Bezeq_Logo.svg.png"
        let rootExtension = Extension(uid: newRootExtensionID, title: "Language", number: "1", isRoot: true, parentUid: newCompanyID, parentExtensionUid: nil)
        let company = Company(uid: newCompanyID, name: "Hot", phoneNumber: "012", imageURL: imageUrl, rootExtensionID: rootExtension.uid)
        FirebaseGlobalManager.add(newExtension: rootExtension)
        FirebaseGlobalManager.add(newCompany: company)
        
        
        
        let option1ChildExtension = Extension(uid: newOption1RootExtensionID, title: "סוג שירות", number: "1", isRoot: false, parentUid: newCompanyID, parentExtensionUid: rootExtension.uid)
        let option1 = Option(uid: newOption1ID, name: "Hebrew", number: "1", parentExtensionID: rootExtension.uid, childExtensionID: option1ChildExtension.uid)
        let option2 = Option(uid: newOption2ID, name: "English", number: "2", parentExtensionID: rootExtension.uid, childExtensionID: option1ChildExtension.uid)
        FirebaseGlobalManager.add(newExtension: option1ChildExtension)
        FirebaseGlobalManager.add(newOption: option1)
        FirebaseGlobalManager.add(newOption: option2)
    }
}
