//
//  ExtensionTableViewController.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 28/04/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import UIKit
import Firebase
import FirebaseUI

class ExtensionTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let showMakeCallSegueIdentifier = "showMakeCall"
    let showOptionSegueIdentifier = "showOption"
    let showCreateOptionSegueIdentifier = "showCreateOption"
    
    var extensionsNumbers: Array<NSNumber?> = []
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "cell"
    
    @IBOutlet weak var tableView: UITableView!
    
    var currentExtension: Extension? = nil
    var options = [Option]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register the table view cell class and its reuse id
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        
        // (optional) include this line if you want to remove the extra empty cell divider lines
        self.tableView.tableFooterView = UIView()
        
        // This view controller itself will provide the delegate methods and row data for the table view.
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        if (Auth.auth().currentUser?.email != "omer.rah2@gmail.com") {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        if let currentID = FirebaseGlobalManager.currentExtensionID {
                  FirebaseGlobalManager.fillExtensionFor(uid: currentID, completion: { extensionResult in
                      DispatchQueue.main.async {
                          if let extensionResult = extensionResult {
                              self.currentExtension = extensionResult
                              self.title = extensionResult.title
                              // TODO:: check if i can extract to parallel function
                            
                          }
                      }
                  })
                  self.options = []
                  FirebaseGlobalManager.fillOptionsFor(extensionID: currentID, completion: { optionResult in
                      if let newOption = optionResult {
                          self.options.append(newOption)
                          self.tableView.reloadData()
                      }
                  })
              }
    }
    
    @IBAction func moreTapped(_ sender: Any) {
        let title = NSLocalizedString("Choose:", comment: "")
        let cancelButtonTitle = NSLocalizedString("Cancel", comment: "")
        let addButtonTitle = NSLocalizedString("Add Option", comment: "")
        let editExtensionButtonTitle = NSLocalizedString("Edit Extension", comment: "")
        //        let editButtonTitle = NSLocalizedString("Edit Extension", comment: "")
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(
            title: NSLocalizedString(cancelButtonTitle, comment: ""),
            style: .cancel) { _ in })
        
        alertController.addAction(UIAlertAction(
            title: NSLocalizedString(addButtonTitle, comment: ""),
            style: .default) { _ in
                self.performSegue(withIdentifier: self.showCreateOptionSegueIdentifier, sender: nil)
        })
        
        alertController.addAction(UIAlertAction(
            title: NSLocalizedString(editExtensionButtonTitle, comment: ""),
            style: .default) { _ in
                self.performSegue(withIdentifier: self.showCreateOptionSegueIdentifier, sender: nil)
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        if let dbRefParent = FirebaseGlobalManager.databaseRef.parent {
        //            FirebaseGlobalManager.databaseRef = dbRefParent
        //        }
        if self.isMovingFromParent {
            FirebaseGlobalManager.currentExtensionID = FirebaseGlobalManager.currentExtension?.parentExtensionUid
        }
    }
    
    // MARK: - UITableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier)!
        cell.accessoryType = .disclosureIndicator
        let extensionOption = self.options[indexPath.row]
        
        cell.textLabel?.text = extensionOption.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedOption = self.options[indexPath.row]
        if (selectedOption.childExtensionID != nil) {
            self.performSegue(withIdentifier: self.showOptionSegueIdentifier, sender: nil)
        } else {
            self.performSegue(withIdentifier: self.showMakeCallSegueIdentifier, sender: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "שלוחה"
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "David", size: 15)
        header.textLabel?.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = tableView.backgroundColor
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
        -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                // delete the item here
                let optionIDToDelete = self.options[indexPath.row].uid
                FirebaseGlobalManager.removeOptionFor(uid: optionIDToDelete, completion: { didRemove in
                    if didRemove {
                        self.tableView.reloadData()
                        completionHandler(true)
                    } else {
                        completionHandler(false)
                    }
                })
                
                //                FirebaseOptionManager.removeOptionForID(uid: optionIDToDelete)
                //                self.options.remove(at: indexPath.row)
            }
            deleteAction.image = UIImage(systemName: "trash")
            deleteAction.backgroundColor = .systemRed
            
            let editAction = UIContextualAction(style: .normal, title: nil) { (_, _, completionHandler) in
                // edit the item here
                completionHandler(true)
            }
            editAction.image = UIImage(systemName: "pencil")
            editAction.backgroundColor = .systemBlue
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction, editAction])
            return configuration
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = self.tableView.indexPathForSelectedRow
        if let index = indexPath?.row {
            let currentOption = options[index]
            //            FirebaseGlobalManager.databaseRef = FirebaseGlobalManager.allExtensions.child(currentOption.uid)
            if (segue.identifier == showOptionSegueIdentifier) {
                FirebaseGlobalManager.currentExtensionID = currentOption.childExtensionID
            } else if (segue.identifier == showMakeCallSegueIdentifier) {
                let vc =  segue.destination as! MakeCallViewController
            }
        }
        if (segue.identifier == showCreateOptionSegueIdentifier) {
            let vc = segue.destination as! CreateOptionViewController
            //            vc.ref = self.ref.child("extensionOptions")
        }
    }
}
