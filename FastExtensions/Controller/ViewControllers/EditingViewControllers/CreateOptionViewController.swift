//
//  CreateExtensionViewController.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 04/05/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import UIKit
import Firebase

class CreateOptionViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var optionNameTextField: UITextField!
    @IBOutlet weak var optionNumberTextField: UITextField!
    
    @IBOutlet weak var extensionTitleTextField: UITextField!
    @IBOutlet weak var extensionNumberTextField: UITextField!
    
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.optionNameTextField.delegate = self
        self.optionNumberTextField.delegate = self
        self.extensionTitleTextField.delegate = self
        self.extensionNumberTextField.delegate = self
        
        self.title = "Create Extension Option"
        // Do any additional setup after loading the view.
        let rightButton = UIBarButtonItem(title: "Save", style: UIBarButtonItem.Style.plain, target: self, action: #selector(saveTapped))
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc private func saveTapped() {
        let optionName = optionNameTextField.text
        let optionNumber = optionNumberTextField.text
        
        let extensionTitle = extensionTitleTextField.text
        let extensionNumber = extensionNumberTextField.text
        
        let parantExtensionID = FirebaseGlobalManager.currentExtension?.uid
        
        let childExtensionKey = FirebaseGlobalManager.generateExtensionKey()
        let optionKey = FirebaseGlobalManager.generateCompanyKey()
        
        if let parantExtensionID = parantExtensionID, let optionKey = optionKey, let childExtensionKey = childExtensionKey {
            let childExtension = Extension(uid: childExtensionKey, title: extensionTitle, number: extensionNumber, isRoot: false, parentUid: optionKey, parentExtensionUid: parantExtensionID)
            
            FirebaseGlobalManager.add(newExtension: childExtension, completion: { didCreateExtension in
                if didCreateExtension {
                    print("Created a new extension: \(childExtension)")
                } else {
                    print("Error - didn't create extension: \(childExtension)")
                }
            })
            
            let newOption = Option(uid: optionKey, name: optionName!, number: optionNumber!, parentExtensionID: parantExtensionID, childExtensionID: childExtensionKey)
            FirebaseGlobalManager.add(newOption: newOption, completion: { didCreateCompany in
                if didCreateCompany {
                    print("Created a new option: \(newOption)")
                    self.navigationController?.popViewController(animated: true)
                } else {
                    print("Error - didn't create company: \(newOption)")
                }
            })
            print("Saved")
        } else {
            print("Didn't create option and extension - Error with creating keys")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
