//
//  CreateCompanyViewController.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 03/05/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import UIKit
import Firebase

class CreateCompanyViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var companyBasePhoneNumberTextField: UITextField!
    @IBOutlet weak var companyImageURLTextField: UITextField!
    @IBOutlet weak var companyImageView: UIImageView!
    
    @IBOutlet weak var extensionTitleTextField: UITextField!
    @IBOutlet weak var extensionNumberTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Create Company"
        
        self.companyNameTextField.delegate = self
        self.companyBasePhoneNumberTextField.delegate = self
        self.companyImageURLTextField.delegate = self
        
        self.extensionTitleTextField.delegate = self
        self.extensionNumberTextField.delegate = self
        
        // Do any additional setup after loading the view.
        let rightButton = UIBarButtonItem(title: "Save", style: UIBarButtonItem.Style.plain, target: self, action: #selector(saveTapped))
        self.navigationItem.rightBarButtonItem = rightButton
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func saveTapped() {
        
        let companyName = companyNameTextField.text
        let companyPhoneNumber = companyBasePhoneNumberTextField.text
        let companyImageURL = companyImageURLTextField.text
        
        let extensionTitle = extensionTitleTextField.text
        let extensionNumber = extensionNumberTextField.text
        
        let extensionKey = FirebaseGlobalManager.generateExtensionKey()
        let companyKey = FirebaseGlobalManager.generateCompanyKey()
        
        if let companyKey = companyKey, let extensionKey = extensionKey {
            let newRootExtension = Extension(uid: extensionKey, title: extensionTitle, number: extensionNumber, isRoot: true, parentUid: companyKey, parentExtensionUid: nil)
            let newCompany = Company(uid: companyKey,name: companyName!,phoneNumber: companyPhoneNumber!,imageURL: companyImageURL!, rootExtensionID: extensionKey)
            
            FirebaseGlobalManager.add(newExtension: newRootExtension, completion: { didCreateExtension in
                if didCreateExtension {
                    print("Created a new extension: \(newRootExtension)")
                } else {
                    print("Error - didn't create extension: \(newRootExtension)")
                }
            })
            
            FirebaseGlobalManager.add(newCompany: newCompany, completion: { didCreateCompany in
                if didCreateCompany {
                    print("Created a new company: \(newCompany)")
                } else {
                    print("Error - didn't create company: \(newCompany)")
                }
            })
        } else {
            print("Didn't create company and extension - Error with creating keys")
        }
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func imageURLEditingChanged(_ sender: Any) {
        if let imageURL = companyImageURLTextField.text {
            self.companyImageView.loadThumbnail(urlSting: imageURL)
        }
    }
    
    func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        companyImageURLTextField.resignFirstResponder()
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        
        let userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 20
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
