//
//  ViewController.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 26/04/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import UIKit
import Firebase

class MakeCallViewController: UIViewController {
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    @IBOutlet weak var makeCallButton: UIButton!
    
    var ref: DatabaseReference!
    
    var phoneNumber = ""
    var descriptionText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO:: change title to English
        self.title = "יצירת קשר"
        
        descriptionTextView.text = descriptionText
        makeCallButton.contentVerticalAlignment = .fill
        makeCallButton.contentHorizontalAlignment = .fill
        //        if let companyImageURL = company.imageURL {
        //            self.imageView.loadThumbnail(urlSting: companyImageURL)
        //        }
        self.navigationController?.setToolbarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingFromParent {
            if let parentViewController = self.navigationController?.topViewController as? ExtensionTableViewController {
                //                parentViewController.extensionsNumbers.removeLast()
            }
        }
    }
    func loadCompany() {
         let dbCompanies = ref.child("companies")
         dbCompanies.observeSingleEvent(of: .value, with: { company in
             if company.childrenCount > 0{
//                 self.snapCompanies.removeAll()
//                 for result in company.children.allObjects as! [DataSnapshot]{
//                     self.snapCompanies.append(result)
//                 }
//                 self.filteredsnapCompanies = self.snapCompanies
//                 self.tableView.reloadData()
             }
         })
     }
    @IBAction func didClickCallPhoneNumberButton(_ sender: Any) {
        callPhoneNumber(phoneNumber: phoneNumber)
    }
    
    private func callPhoneNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
}

