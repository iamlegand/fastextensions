//
//  ViewController.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 26/04/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class LoginViewController: UIViewController, UITextFieldDelegate, GIDSignInDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var facebookSignInButton: UIButton!
    @IBOutlet weak var googleSignInButton: UIButton!
    
    let segueIdentifier = "showCompanyList"
    
    @IBOutlet weak var userEmailTextField: UITextField! {
        didSet {
            let userSymbol = UIImage(systemName: "person")
            userEmailTextField.setIcon(userSymbol!)
        }
    }
    
    @IBOutlet weak var passwordTextField: PasswordTextField! {
        didSet {
            let passwordSymbol = UIImage(systemName: "lock")
            passwordTextField.setIcon(passwordSymbol!)
        }
    }
    
    @IBOutlet weak var phoneNumberTextField: UITextField! {
        didSet {
            let phoneNumber = UIImage(systemName: "phone")
            phoneNumberTextField?.addDoneCancelToolbar()
            phoneNumberTextField.setIcon(phoneNumber!)
        }
    }
    
    // MARK: UIViewController Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerNotifications()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationController?.setToolbarHidden(true, animated: false)
        self.tabBarController?.tabBar.isHidden = true
        
        self.userEmailTextField.delegate = self
        self.passwordTextField.delegate = self
        self.phoneNumberTextField.delegate = self
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        scrollView.contentInset.bottom = 0
    }
    
    // MARK: Buttons Actions
    
    @IBAction func googleSignInPressed(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func facebookSignInButtonPressed(_ sender: Any) {
        FirebasAuthManager.performFacebookLogin(presentingViewController: self, completion: { (success) in
            if success {
                self.performSegue(withIdentifier: self.segueIdentifier, sender: nil)
            }
        })
    }
    
    @IBAction func loginButton(_ sender: Any) {
        if (!self.isValidPhoneNumber(phoneNumber: phoneNumberTextField.text!)) {
            return
        }
        let userEmail = userEmailTextField.text
        if !self.isValidEmail(userEmail) {
            return
        }
        let userPassword = passwordTextField.text
        if !self.isValidPassword(userPassword) {
            return
        }
        let phoneNumber = phoneNumberTextField.text
        if !self.isValidPhoneNumber(phoneNumber: phoneNumber) {
            return
        }
        
        FirebasAuthManager.createUserWithEmail(email: userEmail!, password: userPassword!, completion: { (user) in
            if user != nil {
                self.performSegue(withIdentifier: self.segueIdentifier, sender: nil)
            }
        })
    }
    
    // MARK: Genral Functions
    
    private func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrameValue = notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue else {
            return
        }
        let keyboardFrame = view.convert(keyboardFrameValue.cgRectValue, from: nil)
        scrollView.contentOffset = CGPoint(x:0, y:keyboardFrame.size.height)
    }
    
    @objc private func keyboardWillHide(notification: NSNotification){
        scrollView.contentOffset = .zero
    }
    
    func isValidEmail(_ email: String?) -> Bool {
        guard email != nil else { return false }
        let emailPred = NSPredicate(format:"SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
        return emailPred.evaluate(with: email)
    }
    
    func isValidPassword(_ password:String?) -> Bool {
        guard password != nil else { return false }
        // at least one uppercase,
        // at least one digit
        // at least one lowercase
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: password)
    }
    
    func isValidPhoneNumber(phoneNumber:String?) -> Bool {
        guard phoneNumber != nil else { return false }
        let phoneNumberRegEx = "^\\+?(972|0)(\\-)?0?(([23489]{1}\\d{7})|[5]{1}\\d{8})$"
        let phoneNumberPred = NSPredicate(format:"SELF MATCHES %@", phoneNumberRegEx)
        return phoneNumberPred.evaluate(with: phoneNumber)
    }
    
    // MARK: Delegates
    
    // UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.userEmailTextField.endEditing(true)
        self.passwordTextField.endEditing(true)
        self.phoneNumberTextField.endEditing(true)
        return false
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    // GIDSignInDelegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        //Sign in functionality will be handled here
        if let error = error {
            print(error.localizedDescription)
            return
        }
        guard let auth = user.authentication else { return }
        let credentials = GoogleAuthProvider.credential(withIDToken: auth.idToken, accessToken: auth.accessToken)
        FirebasAuthManager.authenticateToFirebase(credentials, completion: { (success) in
            if success {
                self.performSegue(withIdentifier: self.segueIdentifier, sender: nil)
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        FirebaseGlobalManager.databaseRef = FirebaseGlobalManager.allCompanies
    }
}

