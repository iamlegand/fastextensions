//
//  CompanyTableViewCell.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 29/04/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import UIKit

class CompanyTableViewCell: UITableViewCell {
    var company:Company?
    @IBOutlet weak var companyImage: UIImageView!
    @IBOutlet weak var companyName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureWithType(_ company: Company) {
        self.companyName.text = company.name
        self.companyImage.loadThumbnail(urlSting: company.imageURL)
    }
}
