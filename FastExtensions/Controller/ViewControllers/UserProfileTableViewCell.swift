//
//  UserProfileTableViewCell.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 07/05/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import UIKit

class UserProfileTableViewCell: UITableViewCell {
    var user: FirebaseUser?
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var helloMessageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureWithType(_ user: FirebaseUser) {
        self.userNameLabel.text = user.name
        self.helloMessageLabel.text = "Hey I'm having fun"
        if let userProfileImageURL = user.profileImageURL {
             self.imageView?.loadThumbnail(urlSting: userProfileImageURL)
        }
    }
}
