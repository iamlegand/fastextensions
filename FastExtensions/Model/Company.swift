//
//  Company.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 29/04/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import Foundation
import Firebase

class Company: CustomStringConvertible {
    
    var uid:String
    var rootExtensionID:String?
    var name: String = ""
    var phoneNumber: String = ""
    var imageURL: String = ""
    
    var description: String {
        if let rootExtensionID = rootExtensionID {
            return "company: name = \(name), phoneNumber = \(phoneNumber), imageURL = \(imageURL), uid = \(uid), rootExtensionID = \(rootExtensionID)"
        }
        return "company: name = \(name), phoneNumber = \(phoneNumber), imageURL = \(imageURL), uid = \(uid)"
    }
    
    init(uid:String, name: String, phoneNumber: String, imageURL: String, rootExtensionID: String?) {
        self.uid = uid
        self.rootExtensionID = rootExtensionID
        self.name = name
        self.phoneNumber = phoneNumber
        self.imageURL = imageURL
    }
    
    // Init for reading from Database snapshot
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        self.uid = snapshot.key
        self.rootExtensionID = snapshotValue["rootExtensionID"] as? String
        self.phoneNumber = snapshotValue["phoneNumber"]! as! String
        self.imageURL = snapshotValue["imageURL"]! as! String
        self.name = snapshotValue["name"]! as! String
    }
    
    func toAnyObject() -> Any {
        return [
            "rootExtensionID": self.rootExtensionID as Any,
            "phoneNumber": self.phoneNumber as Any,
            "imageURL": self.imageURL as Any,
            "name": self.name as Any,
        ]
    }
}
