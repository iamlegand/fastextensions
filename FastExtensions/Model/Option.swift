//
//  ExtensionOption.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 29/04/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import Foundation
import Firebase

class Option: CustomStringConvertible {
    
    var uid:String
    var parentExtensionID: String = ""
    var childExtensionID:String?
    var number: String = ""
    var name: String = ""
    
    var description: String {
        if let childExtensionID = childExtensionID {
            return "option: name = \(name), number = \(number), uid = \(uid), parentExtensionID = \(parentExtensionID), childExtensionID = \(childExtensionID)"
        }
        return "option: name = \(name), number = \(number), uid = \(uid), parentExtensionID = \(parentExtensionID)"
    }
    
    init(uid: String, name: String,number: String, parentExtensionID: String, childExtensionID:String?) {
        self.uid = uid
        self.parentExtensionID = parentExtensionID
        self.childExtensionID = childExtensionID
        self.name = name
        self.number = number
    }
    
    // Init for reading from Database snapshot
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        self.uid = snapshot.key
        self.parentExtensionID = snapshotValue["parentExtensionID"] as! String
        self.childExtensionID = snapshotValue["childExtensionID"] as? String
        self.number = snapshotValue["number"] as! String
        self.name = snapshotValue["name"] as! String
    }
    
    // Func converting model for easier writing to database
    func toAnyObject() -> Any {
        return [
            "parentExtensionID": self.parentExtensionID as Any,
            "childExtensionID": self.childExtensionID as Any,
            "number": self.number as Any,
            "name": self.name as Any,
        ]
    }
}
