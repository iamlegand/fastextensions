//
//  Extension.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 29/04/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import Foundation
import Firebase

class Extension: CustomStringConvertible {
    var uid: String!
    var parentUid: String!
    var parentExtensionUid: String?
    var title: String!
    var number: String!
    var isRoot: Bool!

    var description: String {
        if isRoot {
            return "extension: title = \(title!), number = \(number!), uid = \(uid!), parentUid = \(parentUid!), isRoot = \(isRoot!)"
        } else {
            return "extension: title = \(title!), number = \(number!), uid = \(uid!), parentUid = \(parentUid!), isRoot = \(isRoot!), parentExtensionUid = \(parentExtensionUid!)"
        }
    }
    
    init(uid: String, title: String? = nil, number: String? = nil, isRoot: Bool, parentUid: String? = nil, parentExtensionUid: String? = nil) {
        self.uid = uid
        self.parentUid = parentUid
        self.parentExtensionUid = parentExtensionUid
        self.title = title
        self.number = number
        self.isRoot = isRoot
    }
    
    // Init for reading from Database snapshot
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        self.uid = snapshot.key
        self.parentUid = snapshotValue["parentUid"] as? String
        self.parentExtensionUid = snapshotValue["parentExtensionUid"] as? String
        self.title = snapshotValue["title"] as? String
        self.number = snapshotValue["number"] as? String
        self.isRoot = snapshotValue["isRoot"] as? Bool
    }

    func toAnyObject() -> Any {
        return [
            "parentUid": self.parentUid as Any,
            "parentExtensionUid": self.parentExtensionUid as Any,
            "title": self.title as Any,
            "number": self.number as Any,
            "isRoot": self.isRoot as Any
        ]
    }

    //    func inDictionary() -> NSDictionary {
    //        var dict = [String:Any]()
    //        dict["number"] = self.number
    //        dict["title"] = self.title
    //        var optionsDict = [NSDictionary]()
    //        if let extensionOptionsDictionary = self.extensionOptions {
    //            for option in extensionOptionsDictionary {
    //                optionsDict.append(option.inDictionary())
    //            }
    //        }
    //        dict["extensionOptions"] = optionsDict
    //        return dict as NSDictionary
    //    }
}
