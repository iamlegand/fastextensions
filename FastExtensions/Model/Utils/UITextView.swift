//
//  UIView.swift
//  AlbumsBUX
//
//  Created by Omer Rahmany on 11/04/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func setCorner(radius: CGFloat) {
        layer.cornerRadius = radius
        clipsToBounds = true
    }
    
    func circleCorner() {
        superview?.layoutIfNeeded()
        setCorner(radius: frame.height / 2)
    }
}
