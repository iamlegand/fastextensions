//
//  FirebaseGlobalManager.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 07/08/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import Foundation
import Firebase

@objc protocol FIRShowAlertDelegate {
    //    func showFIRAlert(_ message : String)
    @objc optional func activityIndic(_ shouldShow: Bool)
}


class FirebaseGlobalManager : FIRShowAlertDelegate {
    var showAlertDelegate: FIRShowAlertDelegate!
    //    static var databaseRef = Database.database().reference()
    static let db = Database.database(url:"http://localhost:9000/?ns=fastextensions")
    static let databaseRef = db.reference()
    static let allCompanies = FirebaseGlobalManager.databaseRef.child("companies")
    static let allOptions = FirebaseGlobalManager.databaseRef.child("options")
    static let allExtensions = FirebaseGlobalManager.databaseRef.child("extensions")
    static let allExtensionsOptionMembers = FirebaseGlobalManager.databaseRef.child("extensionsOptionMembers")
    
    static var companies = [Company]()
    static var currentCompanyID: String = ""
    static var currentCompany: Company? = nil
    
    static var currentExtensionID: String?
    static var currentExtension: Extension?
    
    static var options = [Option]()
    
    static func generateCompanyKey() -> String? {
        guard let key = FirebaseGlobalManager.allCompanies.childByAutoId().key else {
            return nil
        }
        return key
    }
    
    static func generateExtensionKey() -> String? {
        guard let key = FirebaseGlobalManager.allExtensions.childByAutoId().key else {
            return nil
        }
        return key
    }
    
    static func generateOptionKey() -> String? {
        guard let key = FirebaseGlobalManager.allOptions.childByAutoId().key else {
            return nil
        }
        return key
    }
    
    static func add(newCompany: Company){
        FirebaseGlobalManager.currentCompanyID = newCompany.uid
        print("Adding Company: \(newCompany)")
        FirebaseGlobalManager.allCompanies.child(newCompany.uid).setValue(newCompany.toAnyObject())
    }
    
    static func add(newCompany: Company, completion : @escaping ((_ didAdd : Bool) -> Void)){
        FirebaseGlobalManager.currentCompanyID = newCompany.uid
        print("Adding Company: \(newCompany)")
        FirebaseGlobalManager.allCompanies.child(newCompany.uid).setValue(newCompany.toAnyObject(), withCompletionBlock: { (err, ref) in
            FirebaseGlobalManager.returnCompletion(err, completion: completion)
        })
    }
    
    static func add(newExtension: Extension){
        print("Adding Extension: \(newExtension)")
        FirebaseGlobalManager.allExtensions.child(newExtension.uid).setValue(newExtension.toAnyObject())
    }
    
    static func add(newExtension: Extension, completion : @escaping ((_ didAdd : Bool) -> Void)){
        print("Adding Extension: \(newExtension)")
        FirebaseGlobalManager.allExtensions.child(newExtension.uid).setValue(newExtension.toAnyObject(), withCompletionBlock: { (err, ref) in
            FirebaseGlobalManager.returnCompletion(err, completion: completion)
        })
    }
    
    static func add(newOption:Option){
        print("Adding Option: \(newOption)")
        FirebaseGlobalManager.allOptions.child(newOption.uid).setValue(newOption.toAnyObject())
    }
    
    static func add(newOption:Option, completion : @escaping ((_ didAdd : Bool) -> Void)){
        print("Adding Option: \(newOption)")
        FirebaseGlobalManager.allOptions.child(newOption.uid).setValue(newOption.toAnyObject(), withCompletionBlock: { (err, ref) in
            FirebaseGlobalManager.returnCompletion(err, completion: completion)
        })
    }
    
    static func removeCompanyFor(uid: String, completion : @escaping ((_ didRemove : Bool) -> Void)){
        print("Removing Company: \(uid)")
        FirebaseGlobalManager.allCompanies.child(uid).removeValue(completionBlock: { err, ref in
            FirebaseGlobalManager.returnCompletion(err, completion: completion)
        })
    }
    
    static func removeExtensionsFor(uid: String, completion : @escaping ((_ didRemove : Bool) -> Void)){
        print("Removing Extension: \(uid)")
        FirebaseGlobalManager.allExtensions.child(uid).removeValue(completionBlock: { err, ref in
            FirebaseGlobalManager.returnCompletion(err, completion: completion)
        })
    }
    
    static func removeOptionFor(uid: String, completion : @escaping ((_ didRemove : Bool) -> Void)){
        print("Removing Option: \(uid)")
        FirebaseGlobalManager.allOptions.child(uid).removeValue(completionBlock: { err, ref in
            FirebaseGlobalManager.returnCompletion(err, completion: completion)
        })
    }
    
    
    static func fillCompanies(completion : @escaping ((_ result : Company?) -> Void)) {
        FirebaseGlobalManager.companies = []
        print(FirebaseGlobalManager.allCompanies)
        FirebaseGlobalManager.allCompanies.observe(.childAdded, with: { (snapshot) in
            let newCompany = Company(snapshot: snapshot)
            self.companies.append(newCompany)
            completion(newCompany)
            return
        })
        // TODO:: handle the case where there are no companies and we want to return the completion
    }
    
    
    static func fillExtensionFor(uid:String, completion : @escaping ((_ result : Extension?) -> Void)) {
        print(FirebaseGlobalManager.allExtensions)
        FirebaseGlobalManager.allExtensions.child(uid).queryOrderedByKey().observeSingleEvent(of: .value, with: { (snapshot) in
            let currentExtension = Extension(snapshot:snapshot as DataSnapshot)
            FirebaseGlobalManager.currentExtension = currentExtension
            FirebaseGlobalManager.currentExtensionID = currentExtension.uid
            completion(currentExtension)
            return
        })
    }
    
    static func fillOptionsFor(extensionID:String, completion : @escaping ((_ result : Option?) -> Void)) {
        FirebaseGlobalManager.options = []
        print(FirebaseGlobalManager.allOptions)
        FirebaseGlobalManager.allExtensionsOptionMembers.child(extensionID).queryOrderedByKey().observe(.childAdded, with: { (snapshot) in
            print(snapshot)
            FirebaseGlobalManager.allOptions.child(snapshot.key).observe(.value, with: { optionSnapshot in
                let option = Option(snapshot:optionSnapshot as DataSnapshot)
                FirebaseGlobalManager.options.append(option)
                completion(option)
            })
        })
        completion(nil)
    }
    
    static func doesExtensionExistFor(uid:String, completionHandler: @escaping (_ doesExist:Bool, _ data:Any?) -> ()) {
        FirebaseGlobalManager.allExtensions.child(uid).observeSingleEvent(of: .value) { (snapshot: DataSnapshot) in
            if let data = snapshot.value{
                completionHandler(true, data)
            } else{
                completionHandler(false, nil)
            }
        }
    }
    
    // Utils
    static func returnCompletion(_ err: Error?, completion: (Bool)->()) {
        if (err != nil) {
            print(err.debugDescription)
            completion(false)
        }
        else {
            completion(true)
        }
    }
}


