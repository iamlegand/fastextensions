//
//  FirebaseBaseManager.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 12/08/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import UIKit
import Firebase

@objc protocol FirebaseBaseManagerDelegate {
    //    func showFIRAlert(_ message : String)
    @objc static func generateKey() -> String?
}

class FirebaseBaseManagerDelegate : FirebaseBaseManagerDelegate {
    var databaseRef: DatabaseReference?
    
    static func generateKey() -> String? {
        return nil
    }
}
