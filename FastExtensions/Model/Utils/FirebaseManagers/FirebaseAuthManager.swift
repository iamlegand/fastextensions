//
//  FirebaseDBManager.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 10/05/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import Foundation
import Firebase
import FBSDKLoginKit

class FirebasAuthManager: NSObject {
    static var currentUserID: String = ""
    static var currentUser: User? = nil
    
    static func authenticateToFirebase(_ credentials: AuthCredential,completion: @escaping (_ success:Bool) -> Void) {
        Auth.auth().signIn(with: credentials) { (authResult, error) in
            if let error = error {
                print(error.localizedDescription)
                completion(false)
            } else {
                FirebasAuthManager.writeDataToFirebase(authResult)
                currentUser = authResult?.user
                currentUserID = (currentUser?.uid)!
                completion(true)
            }
        }
    }
    
    static func writeDataToFirebase(_ authResult: AuthDataResult?) {
        let currentUser = authResult?.user
        let userData = ["name": currentUser!.displayName,
                        "email": currentUser!.email,
                        "phoneNumber": currentUser!.phoneNumber]
        let ref = Database.database().reference()
        ref.child("users").setValue([currentUser!.uid : userData])
        print("Create new user in firebase")
    }
    
    //    func printUserData(){
    //        let userID = Auth.auth().currentUser?.uid
    //        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (user) in
    //            // Get user value
    //            let value = user.value as? NSDictionary
    //            let userEmail = value?["email"] as? String ?? ""
    //            let userName = value?["name"] as? String ?? ""
    //            print("user:\(userName) ,with email:\(userEmail) is logged in")
    //            // ...
    //        }) { (error) in
    //            print(error.localizedDescription)
    //        }
    //    }
    
    static func createUserWithEmail(email: String, password:String, completion: @escaping (_ user:User?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            if let authError = error {
                print(authError.localizedDescription)
                completion(nil)
                return
            }
            writeDataToFirebase(authResult)
            currentUser = authResult?.user
            currentUserID = (currentUser?.uid)!
            completion(authResult?.user)
        }
    }
    
    static func performFacebookLogin(presentingViewController: UIViewController, completion: @escaping (_ success:Bool) -> Void){
        let facebookLoginManager = LoginManager()
        facebookLoginManager.logIn(permissions: [], from: presentingViewController) { (result, error) in
            guard error == nil else {
                print(error!.localizedDescription)
                return
            }
            guard let result = result, !result.isCancelled else {
                print("User cancelled login")
                return
            }
            let credentials = FacebookAuthProvider.credential(withAccessToken: AccessToken.current!.tokenString)
            FirebasAuthManager.authenticateToFirebase(credentials, completion: completion)
        }
    }
}
