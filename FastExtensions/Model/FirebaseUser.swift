//
//  User.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 07/05/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class FirebaseUser {
    var name: String?
    var email: String?
    var phoneNumber: String?
    var profileImageURL: String?
    var userSocialID: String?
    
    init(name: String?, email: String?, phoneNumber: String?, profileImageURL:String?, userSocialID: String?) {
        self.name = name
        self.email = email
        self.phoneNumber = phoneNumber
        self.profileImageURL = profileImageURL
        self.userSocialID = userSocialID
    }
    
    // Init for reading from Database snapshot
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        self.name = snapshotValue["name"] as? String
        self.email = snapshotValue["email"] as? String
        self.phoneNumber = snapshotValue["phoneNumber"] as? String
        self.profileImageURL = snapshotValue["profileImageURL"] as? String
        self.userSocialID = snapshotValue["userSocialID"] as? String
    }
    
    func toAnyObject() -> Any {
        return [
            "name": self.name as Any,
            "email": self.email as Any,
            "phoneNumber": self.phoneNumber as Any,
            "profileImageURL": self.profileImageURL as Any,
            "userSocialID": self.userSocialID as Any
        ]
    }
}
