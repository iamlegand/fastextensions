//
//  PasswordTextField.swift
//  FastExtensions
//
//  Created by Omer Rahmany on 26/04/2020.
//  Copyright © 2020 Omer Rahmany. All rights reserved.
//

import UIKit

class PasswordTextField: UITextField {
    
    var rightButton:UIButton!
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        setPasswordShowButton()
    }
    
    func setPasswordShowButton() {
        rightButton = UIButton(type: .custom)
        rightButton.frame = CGRect(x:0, y:0, width:30, height:30)
        rightButton.setImage(UIImage(systemName: "eye"), for: .normal)
        rightButton.addTarget(self, action: #selector(togglePasswordVisibility), for: .touchUpInside)
        self.rightViewMode = .always
        self.rightView = rightButton
    }

    @objc func togglePasswordVisibility() {
        let rightButtonImage:UIImage?
        if isSecureTextEntry {
            rightButtonImage = UIImage(systemName: "eye.slash.fill")
            isSecureTextEntry = false
        } else {
            rightButtonImage = UIImage(systemName: "eye")
            isSecureTextEntry = true
        }
        rightButton.setImage(rightButtonImage, for: .normal)
        
        if let existingText = text, isSecureTextEntry {
            /* When toggling to secure text, all text will be purged if the user
             continues typing unless we intervene. This is prevented by first
             deleting the existing text and then recovering the original text. */
            text = nil
            
            if let textRange = textRange(from: beginningOfDocument, to: endOfDocument) {
                replace(textRange, withText: existingText)
            }
        } else {
            
        }
        
        /* Reset the selected text range since the cursor can end up in the wrong
         position after a toggle because the text might vary in width */
        if let existingSelectedTextRange = selectedTextRange {
            selectedTextRange = nil
            selectedTextRange = existingSelectedTextRange
        }
    }
}
